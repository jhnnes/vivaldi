#!/bin/sh

if [ "$(id -un)" != 0 ]; then
	if [ -f /usr/bin/sudo ]; then
		echo "INFO: Using sudo for root operations."
		root="sudo"
	elif [ -f /usr/bin/doas ]; then
		echo "INFO: Using doas for root operations."
		root="doas"
	fi
fi

echo; echo "Removing all Vivaldi files and directories..."; echo

$root rm -rv /opt/vivaldi
$root rm -rv /etc/cron.daily/vivaldi
$root rm -rv /usr/share/applications/vivaldi-stable.desktop
$root rm -rv /usr/share/appdata/vivaldi.appdata.xml
$root rm -rv /usr/share/doc/vivaldi-stable
$root rm -rv /usr/share/menu/vivaldi.menu
$root rm -rv /usr/share/xfce4/helpers/vivaldi.desktop
$root rm -rv /usr/bin/vivaldi-stable

echo; echo "See you next time. Bye."; echo
